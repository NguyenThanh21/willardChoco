package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.Service.OrderProductService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.orderProduct.ListOrderProductRequest;
import com.haui.chocolateshop.dto.response.orderProduct.OrderProductResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/")
public class OrderProductController extends BaseController{
    @Autowired
    OrderProductService orderProductService;
    @PostMapping("addOrder")
    @ResponseBody
    public ResponseEntity addOrder(@RequestBody BaseRequestData<ListOrderProductRequest> baseRequestData) throws ApplicationException {
        try {
            OrderProductResponse orderRespone = orderProductService.addOrder(baseRequestData);
            return success(orderRespone);
        } catch (ApplicationException e) {
            return error(e.getCode(), e.getMessage());
        }
    }
}
