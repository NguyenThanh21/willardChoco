package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.Service.FooterService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.footer.FooterRequest;
import com.haui.chocolateshop.dto.response.footer.FooterResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@Controller
@RequestMapping("/api")
public class FooterController extends BaseController{
    @Autowired
    FooterService footerService;
    @PostMapping("footer")
    @ResponseBody
    public ResponseEntity footer(@RequestBody BaseRequestData<FooterRequest> baseRequestData) throws ApplicationException{
        try {
            FooterResponse footerResponse= footerService.footer(baseRequestData);
            return success(footerResponse);
        }catch (ApplicationException e){
            return error(e.getCode(),e.getMessage());
        }
    }
}
