package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.Service.ManufactureService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.manufacture.ManufactureRequest;
import com.haui.chocolateshop.dto.response.manufacture.ManufactureResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@Controller
@RequestMapping(value = "/api")
public class ManufactureController extends BaseController{
    @Autowired
    ManufactureService manufactureService;
    @PostMapping("addManufacture")
    @ResponseBody
    public ResponseEntity addManufacture(@RequestBody BaseRequestData<ManufactureRequest> requestData) throws ApplicationException {
        try {
            ManufactureResponse manufactureResponse= manufactureService.addManufacture(requestData);
            return success(manufactureResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("updateManufacture")
    @ResponseBody
    public ResponseEntity updateManufacture(@RequestBody BaseRequestData<ManufactureRequest> requestData) throws ApplicationException {
        try {
            ManufactureResponse manufactureResponse= manufactureService.updateManufacture(requestData);
            return success(manufactureResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("deleteManufacture")
    @ResponseBody
    public ResponseEntity deleteManufacture(@RequestBody BaseRequestData<ManufactureRequest> requestData) throws ApplicationException {
        try {
            ManufactureResponse manufactureResponse= manufactureService.deleteManufacture(requestData);
            return success(manufactureResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("detailManufacture")
    @ResponseBody
    public ResponseEntity detailManufacture(@RequestBody BaseRequestData<ManufactureRequest> requestData) throws ApplicationException {
        try {
            ManufactureResponse manufactureResponse= manufactureService.detailManufacture(requestData);
            return success(manufactureResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
}
