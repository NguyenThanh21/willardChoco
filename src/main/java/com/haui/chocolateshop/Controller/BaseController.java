package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.dto.response.BaseResponseData;
import com.haui.chocolateshop.dto.response.IResponseData;
import com.haui.chocolateshop.utils.MessageUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseController {
    protected  <T extends IResponseData> ResponseEntity success(T respone) {
        BaseResponseData baseResponseData= new BaseResponseData();
        baseResponseData.setErrorCode("success");
       baseResponseData.setMessage(MessageUtils.getMessage("success"));
        baseResponseData.setWsResponse(respone);
        return new ResponseEntity(baseResponseData, HttpStatus.OK);
    }
    protected  <T extends IResponseData> ResponseEntity error(String code, String message) {
        BaseResponseData baseResponseData= new BaseResponseData();
        baseResponseData.setErrorCode(code);
        baseResponseData.setMessage(message);
        return new ResponseEntity(baseResponseData, HttpStatus.OK);
    }

    protected  ResponseEntity success() {
        BaseResponseData baseResponseData= new BaseResponseData();
        baseResponseData.setErrorCode("success");
        baseResponseData.setMessage(MessageUtils.getMessage("success"));
        return new ResponseEntity(baseResponseData, HttpStatus.OK);
    }
}
