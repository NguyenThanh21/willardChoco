package com.haui.chocolateshop.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
@CrossOrigin("*")
@Controller
@RequestMapping(value = "/api")
public class AboutUsController extends BaseController{
}
