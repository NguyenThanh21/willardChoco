package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.Service.ProductService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.category.CategoryRequest;
import com.haui.chocolateshop.dto.request.product.ProductRequest;
import com.haui.chocolateshop.dto.response.category.CategoryResponse;
import com.haui.chocolateshop.dto.response.product.ListProductResponse;
import com.haui.chocolateshop.dto.response.product.ProductResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@Controller
@RequestMapping(value = "/api")
public class ProductController extends BaseController{
    @Autowired
    ProductService productService;

    @PostMapping("addProduct")
    @ResponseBody
    public ResponseEntity addProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        try {
            ProductResponse productResponse= productService.addProduct(requestData);
            return success(productResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("updateProduct")
    @ResponseBody
    public ResponseEntity updateProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        try {
            ProductResponse productResponse= productService.updateProduct(requestData);
            return success(productResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("deleteProduct")
    @ResponseBody
    public ResponseEntity deleteProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        try {
            ProductResponse productResponse= productService.deleteProduct(requestData);
            return success(productResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("detailProduct")
    @ResponseBody
    public ResponseEntity detailProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        try {
            ProductResponse productResponse= productService.detailProduct(requestData);
            return success(productResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("listProduct")
    @ResponseBody
    public ResponseEntity<?> listProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        try {
            ListProductResponse productResponse= productService.listProduct(requestData);
            return success(productResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
}
