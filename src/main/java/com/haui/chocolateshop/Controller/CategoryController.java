package com.haui.chocolateshop.Controller;

import com.haui.chocolateshop.Service.CategoryService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.category.CategoryRequest;
import com.haui.chocolateshop.dto.response.category.CategoryResponse;
import com.haui.chocolateshop.dto.response.category.ListCategoryResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@CrossOrigin("*")
@Controller
@RequestMapping("/api")
public class CategoryController extends BaseController{
    @Autowired
    CategoryService categoryService;

    @PostMapping("getCategory")
    @ResponseBody
    public ResponseEntity getCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        try {
            CategoryResponse categoryResponse= categoryService.getCategory(requestData);
            return success(categoryResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("listCategory")
    @ResponseBody
    public ResponseEntity listCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        try {
            ListCategoryResponse categoryResponse= categoryService.listCategory(requestData);
            return success(categoryResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
    @PostMapping("addCategory")
    @ResponseBody
    public ResponseEntity addCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        try {
            CategoryResponse categoryResponse= categoryService.addCategory(requestData);
            return success(categoryResponse);
        }catch(ApplicationException e){
            return  error(e.getCode(),e.getMessage());
        }
    }
}
