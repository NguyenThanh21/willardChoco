package com.haui.chocolateshop.utils;

import com.haui.chocolateshop.constant.Constant;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Random;


public class StringUtils {

    public static String formatPhone(String number){
        String phone = "0" + number;
        return phone;
    }


    public static String formatIsdnCallWs(String number){
        if (number.startsWith("0")) {
            number = number.replaceFirst("0", "84");
        } else {
            number= "84" + number;
        }
        return number;
    }

    public static String formatPhoneRequest(String number){
        String phone = number;
        if(number.startsWith("84") ){
            phone = number.replaceFirst("84","0");
        } else if(number.startsWith("+84")){
            phone = number.replace("+84","0");
        }
        return phone;
    }

    public static String randOtp(int min, int max) {
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return String.valueOf(randomNum);
        } catch (Exception e) {
            return "";
        }
    }

//    public static int getMinOtp() {
//        return (int) Math.pow(10, Constant.OTP_NUMBER - 1);
//    }
//
//    public static int getMaxOtp() {
//        return (int) (Math.pow(10, Constant.OTP_NUMBER) - 1);
//    }

    public static boolean checkPhone(String number){
        String ser = "^([A-Za-Z])";
        if(number.startsWith("0") || number.startsWith("84") && number.length()==10 && !number.contains(ser)){
            return true;
        }
        return false;
    }

    public  static boolean checkMail(String mail) {
        String rex_mail = "^([a-zA-Z0-9\\.]+)@([\\da-z\\.]+)\\.([a-z\\.]{2,6})$";
        if (mail.matches(rex_mail)) {
            return true;
        }
        return false;
    }

    public static String returnStatusPay(int status){
        if (status==0){
            return "Chưa Thanh Toán";
        }else if (status==1){
            return "Đã Thanh Toán";
        }else return "";
    }

    public static String returnStatusOrder(int status){
        switch (status){
            case 1: return "Chờ Duyệt";
            case 2: return "Đã Duyệt";
            case 3: return "Từ Chối";
            case 4: return "Đã Nhận Hàng";
            case 5: return "Đã Hủy";
            case 6: return "Hoàn Hàng";
            default: return "";
        }
    }


    public static void test() throws ParserConfigurationException, IOException, SAXException {

        String registerData = "\n" +
                "\n" +
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n" +
                "    <soap:Body>\n" +
                "        <create_subs_new_dktt_itcResponse xmlns=\"http://10.156.3.121:8080/pps-info/Services.asmx\">\n" +
                "            <create_subs_new_dktt_itcResult>\n" +
                "                <VNPCLIENT xmlns=\"\">\n" +
                "KT-ERR_001</VNPCLIENT>\n" +
                "            </create_subs_new_dktt_itcResult>\n" +
                "        </create_subs_new_dktt_itcResponse>\n" +
                "    </soap:Body>\n" +
                "</soap:Envelope>";
        Document doc = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(registerData)));
        JSONObject Jobjectregister = new JSONObject(registerData);
        NodeList errNodes = doc.getElementsByTagName("create_subs_new_dktt_itcResult");
        if (errNodes.getLength() > 0) {
            Element err = (Element)errNodes.item(0);
            System.out.println(err.getElementsByTagName("errorMessage")
                    .item(0)
                    .getTextContent());
        }
    }

    private static Document convertStringToXMLDocument(String xmlString)
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        test();
    }
}
