package com.haui.chocolateshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HauiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HauiApplication.class, args);
    }

}
