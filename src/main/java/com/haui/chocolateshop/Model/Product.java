package com.haui.chocolateshop.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "product_code")
    private String name;
    @Column(name = "name")
    private String productCode;
    @Column(name = "category_id")
    private int categoryId;
    @Column(name = "price")
    private double price;
    @Column(name = "description")
    private String description;
    @Column(name = "promotion")
    private int promotion;
    @Column(name = "status")
    private int status;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @Column(name = "update_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @Column(name = "quality")
    private int quality;
    @Column(name = "manufacture_id")
    private int manufactureId;
    @Column(name = "image")
    private String image;
}
