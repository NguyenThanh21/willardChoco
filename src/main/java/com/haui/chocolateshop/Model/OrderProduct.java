package com.haui.chocolateshop.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "order_product")
public class OrderProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "order_code")
    private String orderCode;
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @Column(name = "update_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @Column(name = "amount")
    private double amount;
    @Column(name = "quality")
    private int quality;
    @Column(name = "status_sale")
    private int statusSale;
    @Column(name = "status_pay")
    private int statusPay;
    @Column(name = "cus_id")
    private int cusId;
    @Column(name = "delivery_address")
    private String deliveryAddress;
    @Column(name = "type")
    private int type;
    @Column(name = "pay_method_id")
    private int payMethodId;
}
