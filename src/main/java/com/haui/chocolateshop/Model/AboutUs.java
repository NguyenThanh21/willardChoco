package com.haui.chocolateshop.Model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="about_us")
public class AboutUs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "status")
    private int status;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;
    @Column(name = "imgage")
    private String imgage;
}
