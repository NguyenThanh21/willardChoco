package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.user.UserInfoResponse;
import com.haui.chocolateshop.dto.response.user.UserResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;

public interface UserService {
    UserResponse addUser(BaseRequestData baseRequestData) throws ApplicationException;
    UserResponse updateUser(BaseRequestData baseRequestData) throws ApplicationException;
    UserResponse deleteUser(BaseRequestData baseRequestData) throws ApplicationException;
    UserResponse detailUser(BaseRequestData baseRequestData) throws ApplicationException;
    UserResponse listUser(BaseRequestData baseRequestData) throws ApplicationException;
    UserInfoResponse loginPortal(BaseRequestData baseRequestData) throws ApplicationException;
}
