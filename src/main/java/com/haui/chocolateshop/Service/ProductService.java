package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.product.ListProductResponse;
import com.haui.chocolateshop.dto.response.product.ProductResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;

public interface ProductService {
    ProductResponse addProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ProductResponse updateProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ProductResponse deleteProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ProductResponse detailProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ListProductResponse listProduct(BaseRequestData baseRequestData)throws ApplicationException;
}
