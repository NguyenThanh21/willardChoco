package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.category.CategoryResponse;
import com.haui.chocolateshop.dto.response.category.ListCategoryResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;


public interface CategoryService {
    CategoryResponse getCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse addCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse updateCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse deleteCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse detailCategory(BaseRequestData baseRequestData) throws ApplicationException;
    ListCategoryResponse listCategory(BaseRequestData baseRequestData) throws ApplicationException;
}
