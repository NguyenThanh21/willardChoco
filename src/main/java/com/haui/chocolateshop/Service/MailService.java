package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.Repository.EmailConfigRepository;
import com.haui.chocolateshop.constant.Constant;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.List;
import java.util.Properties;

@Component
@Service
public class MailService {
    @Autowired
    JavaMailSender mailer;

    @Autowired
    EmailConfigRepository emailConfigRepository;

    public Properties properties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Constant.HOST_NAME);
//        props.put("mail.smtp.port", Constant.SSL_PORT);
        props.put("mail.smtp.port", Constant.TSL_PORT);
        return props;
    }
    public void senMail(String email, String pass, String toMail, BodyPart bodyPart, String subject) {
        Properties props = properties();

        javax.mail.Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, pass);
            }
        });
        // compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.CC, toMail);
            message.setSubject(subject);
            //thiết lập nội dụng mail
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            // send message
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    @SneakyThrows
    public BodyPart bodyPart(String content) {
        BodyPart bodyPart = new MimeBodyPart();
        bodyPart.setContent(content, "text/html; charset=UTF-8");
        return bodyPart;
    }
    public  void senMailMulti(String email, String pass, List<String> toMail, BodyPart bodyPart, String subject) {
        Properties props = properties();

        javax.mail.Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, pass);
            }
        });
        // compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, getAddress(toMail));
            message.setSubject(subject);

            //thiết lập nội dụng mail
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            // send message
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public  InternetAddress[] getAddress(List<String> mailAddress) throws AddressException {
        InternetAddress[] addresses = new InternetAddress[mailAddress.size()];
        for (int i = 0; i < mailAddress.size(); i++) {
            InternetAddress internetAddress = new InternetAddress(mailAddress.get(i));
            addresses[i] = internetAddress;
        }
        return addresses;
    }
}
