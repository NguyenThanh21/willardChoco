package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.footer.FooterResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;

public interface FooterService {
    FooterResponse footer(BaseRequestData baseRequestData) throws ApplicationException;
}
