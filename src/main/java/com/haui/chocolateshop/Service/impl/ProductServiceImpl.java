package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.Product;
import com.haui.chocolateshop.Repository.ProductRepository;
import com.haui.chocolateshop.Service.ProductService;
import com.haui.chocolateshop.constant.Constant;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.product.ProductRequest;
import com.haui.chocolateshop.dto.response.product.ListProductResponse;
import com.haui.chocolateshop.dto.response.product.ProductResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.GenCodeUtils;
import com.haui.chocolateshop.utils.MessageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;
    @Override
    public ProductResponse addProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse= new ProductResponse();
        ProductRequest productRequest=(ProductRequest) baseRequestData.getWsRequest();
        try{
            Product product= new Product();
            product.setProductCode(GenCodeUtils.genProductCode());
            product.setCreateTime(LocalDateTime.now());
            product.setCategoryId(productRequest.getCategoryId());
            product.setName(productRequest.getName());
            product.setPrice(productRequest.getPrice());
            product.setPromotion(productRequest.getPromotion());
            product.setStatus(productRequest.getStatus());
            product.setDescription(productRequest.getDescription());
            product.setCreateUser(productRequest.getCreateUser());
            product.setQuality(productRequest.getQuality());
            product.setManufactureId(productRequest.getManufactureId());
            product.setImage(productRequest.getImage());
            productRepository.save(product);
            BeanUtils.copyProperties(product,productResponse);
        }catch (Exception e){
            throw new ApplicationException("error");
        }
        return productResponse;
    }

    @Override
    public ProductResponse updateProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse= new ProductResponse();
        ProductRequest productRequest=(ProductRequest) baseRequestData.getWsRequest();
        try{
            Product product= productRepository.getById(productRequest.getId());
            if(ObjectUtils.isEmpty(product)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Product "+product.getName()));
            }
            product.setProductCode(productRequest.getProductCode());
            product.setUpdateTime(LocalDateTime.now());
            product.setQuality(productRequest.getQuality());
            product.setImage(productRequest.getImage());
            product.setPromotion(productRequest.getPromotion());
            product.setPrice(productRequest.getPrice());
            product.setCategoryId(productRequest.getCategoryId());
            product.setDescription(productRequest.getDescription());
            productRepository.save(product);
            BeanUtils.copyProperties(product,productResponse);
        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return productResponse;
    }

    @Override
    public ProductResponse deleteProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse= new ProductResponse();
        ProductRequest productRequest=(ProductRequest) baseRequestData.getWsRequest();
        try{
            Product product= productRepository.getById(productRequest.getId());
            if(ObjectUtils.isEmpty(product)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Product "+product.getName() ));
            }
            if(product.getStatus()!= Constant.DELETE){
                product.setStatus(Constant.DELETE);
                productRepository.save(product);
                BeanUtils.copyProperties(product,productResponse);
            }else {
                throw new ApplicationException("ERR0000102",MessageUtils.getMessage("product "+product.getName()));
            }
        }catch (ApplicationException e){
            e.getLocalizedMessage();
            throw e;
        }
        return productResponse;
    }

    @Override
    public ProductResponse detailProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse= new ProductResponse();
        ProductRequest productRequest=(ProductRequest) baseRequestData.getWsRequest();
        try{
            Product product= productRepository.getById(productRequest.getId());
            if(ObjectUtils.isEmpty(product)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Product "+product.getName() ));
            }
            BeanUtils.copyProperties(product,productResponse);
        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return productResponse;
    }

    @Override
    public ListProductResponse listProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ListProductResponse listProductResponse= new ListProductResponse();
        List<ProductResponse> productResponseList= new ArrayList<>();
        ProductRequest productRequest=(ProductRequest) baseRequestData.getWsRequest();
        try{
            List<Product> productList= productRepository.listProduct(productRequest.getCategoryId(),productRequest.getName(),
                    PageRequest.of(productRequest.getPage(), productRequest.getPageSize()));
            for(Product product:productList){
                ProductResponse productResponse= new ProductResponse();
                BeanUtils.copyProperties(product,productResponse);
                productResponseList.add(productResponse);
            }
            listProductResponse.setProductResponseList(productResponseList);
            int totalItem= productRepository.countListProduct(productRequest.getCategoryId(),productRequest.getName());
            int totalPage=(int)Math.ceil((double) totalItem/productRequest.getPageSize());
            listProductResponse.setTotalItem(totalItem);
            listProductResponse.setTotalPage(totalPage);
        }catch (Exception e){
            throw new ApplicationException("error");
        }
        return listProductResponse;
    }
}
