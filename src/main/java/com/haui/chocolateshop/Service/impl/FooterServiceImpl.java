package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.Footer;
import com.haui.chocolateshop.Repository.FooterRepository;
import com.haui.chocolateshop.Service.FooterService;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.footer.FooterResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.MessageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class FooterServiceImpl implements FooterService {
    @Autowired
    FooterRepository footerRepository;
    @Override
    public FooterResponse footer(BaseRequestData baseRequestData) throws ApplicationException {
        FooterResponse footerResponse= new FooterResponse();
        try{
            Footer footer= footerRepository.footer();
            if(ObjectUtils.isEmpty(footer)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Footer"));
            }
            BeanUtils.copyProperties(footer,footerResponse);
        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return footerResponse;
    }
}
