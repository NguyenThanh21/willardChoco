package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.Manufacture;
import com.haui.chocolateshop.Repository.ManufactureRepository;
import com.haui.chocolateshop.Service.ManufactureService;
import com.haui.chocolateshop.constant.Constant;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.manufacture.ManufactureRequest;
import com.haui.chocolateshop.dto.response.manufacture.ListManufactureResponse;
import com.haui.chocolateshop.dto.response.manufacture.ManufactureResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.MessageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;

@Service
public class ManufactureServiceImpl implements ManufactureService {
    @Autowired
    ManufactureRepository manufactureRepository;
    @Override
    public ManufactureResponse addManufacture(BaseRequestData baseRequestData) throws ApplicationException {
        ManufactureResponse manufactureResponse= new ManufactureResponse();
        ManufactureRequest manufactureRequest= (ManufactureRequest) baseRequestData.getWsRequest();
        try{
            Manufacture manufacture= new Manufacture();
            manufacture.setCreateTime(LocalDateTime.now());
            manufacture.setName(manufactureRequest.getName());
            manufacture.setStatus(Constant.ACTIVE);
            manufacture.setNationName(manufactureRequest.getNationName());
            manufacture.setDescription(manufactureRequest.getDescription());
            manufactureRepository.save(manufacture);
            BeanUtils.copyProperties(manufacture,manufactureResponse);
        }catch (Exception e){
            throw new ApplicationException("error");
        }
        return manufactureResponse;
    }

    @Override
    public ManufactureResponse updateManufacture(BaseRequestData baseRequestData) throws ApplicationException {
        ManufactureResponse manufactureResponse= new ManufactureResponse();
        ManufactureRequest manufactureRequest= (ManufactureRequest) baseRequestData.getWsRequest();
        try{
            Manufacture manufacture= manufactureRepository.getById(manufactureRequest.getId());
            if(ObjectUtils.isEmpty(manufacture)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Manufacture id"));
            }
            manufacture.setUpdateTime(LocalDateTime.now());
            manufacture.setName(manufactureRequest.getName());
            manufacture.setStatus(Constant.ACTIVE);
            manufacture.setNationName(manufactureRequest.getNationName());
            manufacture.setDescription(manufactureRequest.getDescription());
            manufactureRepository.save(manufacture);
            BeanUtils.copyProperties(manufacture,manufactureResponse);
        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return manufactureResponse;
    }

    @Override
    public ManufactureResponse deleteManufacture(BaseRequestData baseRequestData) throws ApplicationException {
        ManufactureResponse manufactureResponse= new ManufactureResponse();
        ManufactureRequest manufactureRequest= (ManufactureRequest) baseRequestData.getWsRequest();
        try{
            Manufacture manufacture= manufactureRepository.getById(manufactureRequest.getId());
            if(ObjectUtils.isEmpty(manufacture)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Manufacture id"));
            }
            if(manufacture.getStatus()!=Constant.ACTIVE){
                manufacture.setStatus(Constant.ACTIVE);
                manufacture.setUpdateTime(LocalDateTime.now());
                manufactureRepository.save(manufacture);
                BeanUtils.copyProperties(manufacture,manufactureResponse);
            }else {
                throw new ApplicationException("ERR0000102",MessageUtils.getMessage("Manufacture"));
            }
        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return manufactureResponse;
    }

    @Override
    public ManufactureResponse detailManufacture(BaseRequestData baseRequestData) throws ApplicationException {
        ManufactureResponse manufactureResponse= new ManufactureResponse();
        ManufactureRequest manufactureRequest= (ManufactureRequest) baseRequestData.getWsRequest();
        try{
            Manufacture manufacture= manufactureRepository.getById(manufactureRequest.getId());
            if(ObjectUtils.isEmpty(manufacture)){
                throw new ApplicationException("ERR0000100", MessageUtils.getMessage("Manufacture id"));
            }
                BeanUtils.copyProperties(manufacture,manufactureResponse);

        }catch (ApplicationException e){
            throw new ApplicationException("error");
        }
        return manufactureResponse;
    }

    @Override
    public ListManufactureResponse listManufacture(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }
}
