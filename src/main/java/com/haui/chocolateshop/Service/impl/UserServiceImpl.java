package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.Customer;
import com.haui.chocolateshop.Model.User;
import com.haui.chocolateshop.Repository.CustomerRepository;
import com.haui.chocolateshop.Repository.UserRepository;
import com.haui.chocolateshop.Service.UserService;
import com.haui.chocolateshop.constant.Constant;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.user.UserRequest;
import com.haui.chocolateshop.dto.response.user.UserInfoResponse;
import com.haui.chocolateshop.dto.response.user.UserResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.GenCodeUtils;
import com.haui.chocolateshop.utils.MessageUtils;
import com.haui.chocolateshop.utils.PasswordEncryption;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    HttpServletRequest httpServletRequest;
    @Override
    public UserResponse addUser(BaseRequestData baseRequestData) throws ApplicationException {
        UserResponse userRespone = new UserResponse();
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try{
            User checkMail = userRepository.findUserByEmail(userRequest.getEmail());
            User checkPhone = userRepository.findUserByPhone(userRequest.getPhone());
            if (!ObjectUtils.isEmpty(checkMail)) {
                throw new ApplicationException("ERR_0000501", MessageUtils.getMessage("email"));
            }
            if (!ObjectUtils.isEmpty(checkPhone)) {
                throw new ApplicationException("ERR_0000005", MessageUtils.getMessage("Phone"));
            }
            User userr = userRepository.findByUsername(userRequest.getUsername());
            if (ObjectUtils.isEmpty(userr)) {
                Customer customer= new Customer();
                customer.setStatus(Constant.ACTIVE);
                customer.setCreateTime(LocalDateTime.now());

                customerRepository.save(customer);
                User user = new User();
                BeanUtils.copyProperties(userRequest, user);
                user.setStatusUser(Constant.ACTIVE);
                user.setCreateTime(LocalDateTime.now());
                user.setPhone(userRequest.getPhone());
                user.setRoleId(userRequest.getRoleId());
                user.setUsername(userRequest.getUsername());
                user.setPassword(PasswordEncryption.encryteBCryptPassword(userRequest.getPassword()));
                user.setPassword(PasswordEncryption.encryteBCryptPassword(userRequest.getPassRetype()));
                user.setCusId(customer.getCusId());
                userRepository.save(user);
                BeanUtils.copyProperties(user, userRespone);
            }else {
                throw new ApplicationException("ERR0000101",MessageUtils.getMessage("username"));
            }
        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }
        return userRespone;
    }


    @Override
    public UserResponse updateUser(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }

    @Override
    public UserResponse deleteUser(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }

    @Override
    public UserResponse detailUser(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }

    @Override
    public UserResponse listUser(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }

    @Override
    public UserInfoResponse loginPortal(BaseRequestData baseRequestData) throws ApplicationException {
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findByUsername(userRequest.getUsername());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException("ERR_0000603", MessageUtils.getMessage("username"));
            }
            boolean checkUser = PasswordEncryption.bCryptPasswordEncoder(userRequest.getPassword(), user.getPassword());
            if (checkUser) {
                if (user.getRoleId() !=Constant.ROLE_CUSTOMER) {
                    userInfoResponse = new UserInfoResponse();
                    String tokenData = GenCodeUtils.encrypt(user.getUsername() + "_" + user.getRoleId() + "_" + user.getId(), Constant.KEY, Constant.SECRET_KEY);
                    userInfoResponse.setToken(tokenData);
                    user.setToken(tokenData);
                    HttpSession session = httpServletRequest.getSession();
                    session.setMaxInactiveInterval(60*60*24);
                    user.setSession(session.getId());
                    userRepository.save(user);
                    BeanUtils.copyProperties(user, userInfoResponse);
                }
            } else {
                throw new ApplicationException("ERR_0000002");
            }
        } catch (ApplicationException e) {
            e.getLocalizedMessage();
        }
        return userInfoResponse;
    }
}
