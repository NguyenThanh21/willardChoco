package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.*;
import com.haui.chocolateshop.Repository.*;
import com.haui.chocolateshop.Service.MailService;
import com.haui.chocolateshop.Service.OrderProductService;
import com.haui.chocolateshop.constant.Constant;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.orderProduct.ListOrderProductRequest;
import com.haui.chocolateshop.dto.request.orderProduct.OrderProductRequest;
import com.haui.chocolateshop.dto.response.orderProduct.OrderProductResponse;
import com.haui.chocolateshop.dto.response.saleTrans.PriceSaleTransResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.GenCodeUtils;
import com.haui.chocolateshop.utils.MessageUtils;
import com.haui.chocolateshop.utils.PasswordEncryption;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.mail.BodyPart;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class OrderProductServiceImpl implements OrderProductService {
    @Autowired
    OrderProductRepository orderProductRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    SaleTransRepository saleTransRepository;
    @Autowired
    SaleTransDetailRepository saleTransDetailRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    EmailConfigRepository emailConfigRepository;
    @Autowired
    MailService mailService;

    @Transactional(rollbackFor = ApplicationException.class)
    @Override
    public OrderProductResponse addOrder(BaseRequestData baseRequestData) throws ApplicationException {
        OrderProductResponse orderProductResponse = new OrderProductResponse();
        ListOrderProductRequest listOrderProductRequest = (ListOrderProductRequest) baseRequestData.getWsRequest();
        try {
            if (StringUtils.isEmpty(listOrderProductRequest.getPhone())) {
                throw new ApplicationException("ERR0000103", MessageUtils.getMessage("PhoneNumber"));
            }
            if (StringUtils.isEmpty(listOrderProductRequest.getEmail())) {
                throw new ApplicationException("ERR0000103", MessageUtils.getMessage("email"));
            }
            if (StringUtils.isEmpty(listOrderProductRequest.getCusName())) {
                throw new ApplicationException("ERR0000103", MessageUtils.getMessage("cusName"));
            }
//            if (ObjectUtils.isEmpty(listOrderProductRequest.getOrderProductRequestList())) {
//                throw new ApplicationException("ERR_000104");
//            }
            User user = null;
            String pass = "";
            Customer customer = customerRepository.findCustomerByCustomerEmail(listOrderProductRequest.getEmail());
//            nếu không trùng thì tạo mới khách hàng
            if (ObjectUtils.isEmpty(customer)) {
                customer = new Customer();
                BeanUtils.copyProperties(listOrderProductRequest, customer);
                customer.setStatus(Constant.ACTIVE);
                customer.setCreateTime(LocalDateTime.now());
                customerRepository.save(customer);
                user = new User();
                String username = customer.getEmail().substring(0, customer.getEmail().indexOf("@"));
                User userCheck = userRepository.findByUsername(username);
                if (!ObjectUtils.isEmpty(userCheck)) {
                    user.setUsername(username + customer.getCusId());
                }
                User userCheckMail = userRepository.findUserByEmail(customer.getEmail());
                if (!ObjectUtils.isEmpty(userCheckMail)) {
                    user.setEmail(customer.getEmail() + customer.getCusId());
                } else {
                    user.setEmail(customer.getEmail());
                }
                user.setUsername(username);
                user.setCusId(customer.getCusId());
                user.setPhone(customer.getPhone());
                pass = "Yousim@" + customer.getCusId();
                user.setPassword(PasswordEncryption.encryteBCryptPassword(pass));
                user.setStatusUser(Constant.ACTIVE);
                user.setCreateTime(LocalDateTime.now());
                user.setUpdateTime(LocalDateTime.now());
                userRepository.save(user);
            }
//            Nếu đã tồn tại thì update thông tin mới khách mới nhập
            customer.setCusName(listOrderProductRequest.getCusName());
            customer.setPhone(listOrderProductRequest.getPhone());
            customer.setAddress(listOrderProductRequest.getAddress());
            customer.setUpdateTime(LocalDateTime.now());
            customerRepository.save(customer);
            OrderProduct orderProduct = new OrderProduct();
            BeanUtils.copyProperties(listOrderProductRequest, orderProduct);
            if (!StringUtils.isEmpty(listOrderProductRequest.getAddress())) {
                orderProduct.setDeliveryAddress(listOrderProductRequest.getAddress());
            }
            orderProduct.setOrderCode(GenCodeUtils.genOrderCode());
            orderProduct.setCusId(customer.getCusId());
            orderProduct.setStatusSale(Constant.NEW_ORDER);
            orderProduct.setStatusPay(Constant.UNPAID);
            orderProduct.setCreateTime(LocalDateTime.now());
            orderProduct.setUpdateTime(LocalDateTime.now());
            orderProduct.setType(Constant.BUY_AGENCY);
            orderProduct.setPayMethodId(listOrderProductRequest.getPayMethodId());
            orderProductRepository.save(orderProduct);
//            tổng tiền toàn bộ đơn hàng
            double totalAmount = 0;
//            Tổng tiền sản phẩm
            double amountProduct = 0;
            int buyNumber = 0;
            List<SaleTrans> saleTransList = new ArrayList<>();
            List<Product> productList = new ArrayList<>();
            SaleTrans saleTrans = null;
            HashMap<Integer, SaleTrans> saleTransHashMap = new HashMap<>();
            for (OrderProductRequest orderProductRequest : listOrderProductRequest.getOrderProductRequestList()) {

                double amount = totalAmount(orderProductRequest.getQuality(), orderProductRequest.getIdProduct()).getAmount();
                Product product = productRepository.getById(orderProductRequest.getIdProduct());
                if (product.getQuality() <= 0) {
                    throw new ApplicationException("ERR_000105", MessageUtils.getMessage("Sản phẩm ") + product.getName());
                }
                if (product.getQuality() >= orderProductRequest.getQuality()) {
                    if (ObjectUtils.isEmpty(saleTrans)) {
                        saleTrans = new SaleTrans();
                        saleTrans.setCreateTime(LocalDateTime.now());
                        saleTrans.setUpdateTime(LocalDateTime.now());
                        saleTrans.setOrderId(orderProduct.getId());
                        saleTrans.setProductId(orderProduct.getId());
                        saleTrans.setManufactureId(product.getManufactureId());
                    }
                    SaleTrans setSaleTrans= setSaleTrans(orderProductRequest,amount,product,orderProduct);
                    saleTransList.add(setSaleTrans);
                    buyNumber += orderProductRequest.getQuality();
                    product.setUpdateTime(LocalDateTime.now());
                    amountProduct += amount;
                    int number= product.getQuality() - buyNumber;
                    product.setQuality(number);
                    productRepository.save(product);
                    productList.add(product);
                } else {
                    continue;
                }
                totalAmount = amountProduct;
            }
            if (productList.size() > 0) {
                productRepository.saveAll(productList);
            }
            if (saleTransList.size() > 0) {
                saleTransRepository.saveAll(saleTransList);
            }
            orderProduct.setAmount(totalAmount);
            orderProduct.setQuality(buyNumber);
            orderProductRepository.save(orderProduct);
            if (!ObjectUtils.isEmpty(saleTrans)) {
                saleTrans.setAmount(amountProduct);
                saleTrans.setQuality(buyNumber);
            }
            BeanUtils.copyProperties(orderProduct, orderProductResponse);
//            if (!ObjectUtils.isEmpty(user)) {
//                EmailConfig emailConfig = emailConfigRepository.findByAction(Constant.NEW_MEMBER);
//                if (!ObjectUtils.isEmpty(emailConfig)) {
//                    String content = emailConfig.getContent();
//                    content = content.replace("abc", user.getEmail());
//                    content = content.replace("luongdd", pass);
//                    BodyPart bodyPart = mailService.bodyPart(content);
//                    mailService.senMail(emailConfig.getEmail(), emailConfig.getPassword(), user.getEmail(), bodyPart, emailConfig.getSubject());
//                }
//            }
        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }
        return orderProductResponse;
    }

    private SaleTrans setSaleTrans( OrderProductRequest orderRequest, double amount,Product product,OrderProduct orderProduct) {
        SaleTrans saleTrans = new SaleTrans();
        saleTrans.setQuality(orderRequest.getQuality());
        saleTrans.setCreateTime(LocalDateTime.now());
        saleTrans.setUpdateTime(LocalDateTime.now());
        saleTrans.setProductId(orderRequest.getIdProduct());
        saleTrans.setAmount(amount);
        saleTrans.setManufactureId(product.getManufactureId());
        saleTrans.setOrderId(orderProduct.getId());
        return saleTrans;
    }

    private PriceSaleTransResponse totalAmount(int quality, int idProduct) {
        PriceSaleTransResponse priceSaleTranRespone = new PriceSaleTransResponse();
        double amount = 0;
        Product product = productRepository.getById(idProduct);
        if (!ObjectUtils.isEmpty(product)) {
            if (product.getPromotion() == 0) {
                amount += quality * product.getPrice();
                priceSaleTranRespone.setAmount(amount);
                priceSaleTranRespone.setIdProduct(product.getId());
            } else {
                amount += (quality * product.getPrice()) * (1 - (product.getPromotion() / 100));
                priceSaleTranRespone.setAmount(amount);
                priceSaleTranRespone.setPromotion(product.getPromotion());
                priceSaleTranRespone.setIdProduct(product.getId());
            }
        }
        return priceSaleTranRespone;
    }

    @Override
    public OrderProductResponse updateOrder(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }
}
