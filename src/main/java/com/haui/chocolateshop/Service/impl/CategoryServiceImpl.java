package com.haui.chocolateshop.Service.impl;

import com.haui.chocolateshop.Model.Category;
import com.haui.chocolateshop.Repository.CategoryRepository;
import com.haui.chocolateshop.Service.CategoryService;
import com.haui.chocolateshop.constant.Constant;
import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.request.category.CategoryRequest;
import com.haui.chocolateshop.dto.response.category.CategoryResponse;
import com.haui.chocolateshop.dto.response.category.ListCategoryResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;
import com.haui.chocolateshop.utils.MessageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;
    @Override
    public CategoryResponse getCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse= new CategoryResponse();
        CategoryRequest categoryRequest=(CategoryRequest) baseRequestData.getWsRequest();
        try{
            Category category= categoryRepository.getCategoryById(categoryRequest.getId());
            if(ObjectUtils.isEmpty(category)){
                throw  new ApplicationException("ERR0000100", MessageUtils.getMessage("Category"));
            }
            BeanUtils.copyProperties(category,categoryResponse);
        }catch (ApplicationException e){
            e.getLocalizedMessage();
            throw e;
        }
        return categoryResponse;
    }

    @Override
    public CategoryResponse addCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse= new CategoryResponse();
        CategoryRequest categoryRequest=(CategoryRequest) baseRequestData.getWsRequest();
        try{
            Category category= new Category();
            category.setName(categoryRequest.getName());
            category.setStatus(Constant.ACTIVE);
            category.setLanguage(String.valueOf(LocaleContextHolder.getLocale()));
            categoryRepository.save(category);
            BeanUtils.copyProperties(category,categoryResponse);
        }catch (Exception e){
            throw new ApplicationException("error");
        }
        return categoryResponse;
    }

    @Override
    public CategoryResponse updateCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse= new CategoryResponse();
        CategoryRequest categoryRequest=(CategoryRequest) baseRequestData.getWsRequest();
        try{
            Category category= categoryRepository.getCategoryById(categoryRequest.getId());
            if(ObjectUtils.isEmpty(category)){
                throw  new ApplicationException("ERR0000100", MessageUtils.getMessage("Category"));
            }
            category.setName(categoryRequest.getName());
            category.setStatus(Constant.ACTIVE);
            categoryRepository.save(category);
            BeanUtils.copyProperties(category,categoryResponse);
        }catch (ApplicationException e){
            e.getLocalizedMessage();
            throw e;
        }
        return categoryResponse;
    }

    @Override
    public CategoryResponse deleteCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse= new CategoryResponse();
        CategoryRequest categoryRequest=(CategoryRequest) baseRequestData.getWsRequest();
        try{
            Category category= categoryRepository.getCategoryById(categoryRequest.getId());
            if(ObjectUtils.isEmpty(category)){
                throw  new ApplicationException("ERR0000100", MessageUtils.getMessage("Category"));
            }
            if(category.getStatus()!=Constant.DELETE){
                category.setStatus(Constant.DELETE);
                categoryRepository.save(category);
                BeanUtils.copyProperties(category,categoryResponse);
            }else {
                throw new ApplicationException("ERR0000102",MessageUtils.getMessage("Category"));
            }
        }catch (ApplicationException e){
            e.getLocalizedMessage();
            throw e;
        }
        return categoryResponse;
    }

    @Override
    public CategoryResponse detailCategory(BaseRequestData baseRequestData) throws ApplicationException {
        return null;
    }

    @Override
    public ListCategoryResponse listCategory(BaseRequestData baseRequestData) throws ApplicationException {
        ListCategoryResponse listCategoryResponse= new ListCategoryResponse();
        List<CategoryResponse> categoryResponseList= new ArrayList<>();
        try{
            List<Category> categoryList= categoryRepository.findAll();
            for (Category category:categoryList){
                CategoryResponse categoryResponse= new CategoryResponse();
                BeanUtils.copyProperties(category,categoryResponse);
                categoryResponseList.add(categoryResponse);
            }
            listCategoryResponse.setCategoryResponseList(categoryResponseList);
        }catch (Exception e){
           throw new ApplicationException("error");
        }
        return listCategoryResponse;
    }
}
