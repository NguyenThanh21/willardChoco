package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.orderProduct.OrderProductResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;

public interface OrderProductService {
    OrderProductResponse addOrder(BaseRequestData baseRequestData) throws ApplicationException;
    OrderProductResponse updateOrder(BaseRequestData baseRequestData) throws ApplicationException;
}
