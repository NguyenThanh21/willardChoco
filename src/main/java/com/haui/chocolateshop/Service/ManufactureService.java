package com.haui.chocolateshop.Service;

import com.haui.chocolateshop.dto.request.BaseRequestData;
import com.haui.chocolateshop.dto.response.manufacture.ListManufactureResponse;
import com.haui.chocolateshop.dto.response.manufacture.ManufactureResponse;
import com.haui.chocolateshop.utils.Exception.ApplicationException;

public interface ManufactureService {
    ManufactureResponse addManufacture(BaseRequestData baseRequestData) throws ApplicationException;
    ManufactureResponse updateManufacture(BaseRequestData baseRequestData) throws ApplicationException;
    ManufactureResponse deleteManufacture(BaseRequestData baseRequestData) throws ApplicationException;
    ManufactureResponse detailManufacture(BaseRequestData baseRequestData) throws ApplicationException;
    ListManufactureResponse listManufacture(BaseRequestData baseRequestData) throws ApplicationException;
}
