package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "select u from User u where u.email = ?1")
    User findUserByEmail(String email);

    @Query(value = "select u from User u where u.phone = ?1")
    User findUserByPhone(String phone);

    @Query(value = "select u from User u where u.username = ?1 and u.statusUser = 1")
    User findByUsername(String username);

}
