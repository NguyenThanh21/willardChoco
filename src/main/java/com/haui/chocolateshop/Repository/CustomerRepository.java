package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    @Query(value = "select u from Customer u where u.email= ?1")
    Customer findCustomerByCustomerEmail(String email);
}
