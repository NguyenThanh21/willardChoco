package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.Manufacture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufactureRepository extends JpaRepository<Manufacture,Integer> {
}
