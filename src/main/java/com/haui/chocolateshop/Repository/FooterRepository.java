package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.Footer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FooterRepository extends JpaRepository<Footer,Integer> {
    @Query(value = "select * from footer f order by f.create_time DESC Limit 1 ",nativeQuery = true)
    Footer footer();
}
