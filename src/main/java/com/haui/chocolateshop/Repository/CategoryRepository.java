package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    @Query(value = "select c from Category c where c.id=:id")
    Category getCategoryById(@Param("id") int id);

}
