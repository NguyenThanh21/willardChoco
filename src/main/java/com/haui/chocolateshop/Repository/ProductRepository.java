package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {
    @Query(value = "select p from Product p where p.status= 1 and (-1 in :categoryId or p.categoryId in :categoryId) " +
            "and (:name is null or lower(p.name) LIKE lower(concat('%', concat(:name, '%')))) order by p.createTime DESC ")
    List<Product> listProduct(@Param("categoryId")int categoryId, @Param("name")String name, Pageable pageable);
    @Query(value = "select count(p) from Product p where p.status= 1 and (-1 in :categoryId or p.categoryId in :categoryId) " +
            "and (:name is null or lower(p.name) LIKE lower(concat('%', concat(:name, '%')))) order by p.createTime DESC ")
    int countListProduct(@Param("categoryId")int categoryId, @Param("name")String name);
}
