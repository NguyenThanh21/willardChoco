package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.SaleTransDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleTransDetailRepository extends JpaRepository<SaleTransDetail,Integer> {
}
