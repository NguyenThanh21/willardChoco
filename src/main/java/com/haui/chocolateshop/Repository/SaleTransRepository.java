package com.haui.chocolateshop.Repository;

import com.haui.chocolateshop.Model.SaleTrans;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleTransRepository extends JpaRepository<SaleTrans,Integer> {
}
