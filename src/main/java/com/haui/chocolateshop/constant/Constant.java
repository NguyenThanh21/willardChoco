package com.haui.chocolateshop.constant;

public class Constant {
    public static final String SECRET_KEY = "f12jfh5h3h6k4k2k4jd3nx";
    public static final String KEY = "0000000000";
    public static int ACTIVE=1;
    public static int DELETE=2;
    public static final int ALL = -1;
    public static int SUCCESS = 1;
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";
    //    validate email
    public static final String EMAIL_PATTERN = "^([a-zA-Z0-9\\.]+)@([\\da-z\\.]+)\\.([a-z\\.]{2,6})$";

    public static String PASSWORD_DEFAULT = "Chocolate@";

    // role user
    public static int ROLE_ADMIN = 1;
    public static int ROLE_STAFF = 2;
    public static int ROLE_CUSTOMER = 3;
    public static int SUPER_ADMIN = 10;
    // Config mail
    public static final String HOST_NAME = "smtp.gmail.com";
    public static final int SSL_PORT = 465; // Port for SSL
    public static final int TSL_PORT = 587; // Port for TLS/STARTTLS
    //Loại mua
    public static final int BUY_CHOCOLATE=1;
    public static final int BUY_CANDY=2;
    public static final int BUY_CAKE=3;

    //trang thai don hang
    public static final int NEW_ORDER = 1;// chưa duyệt
    public static final int CONFIRMED_ORDER = 2;// xác nhận đơn hàng
    public static final int REFUSE_ORDER = 3;// từ chối đặt hàng
    public static final int RECEIVED_ORDER = 4;// đã nhận đơn hàng
    public static final int CANCELED_ORDER = 5;// huỷ đơn hàng
    public static final int REFUND_ORDER = 6;// GIAO HÀNG KHÔNG THÀNH CÔNG, ĐƠN HÀNG BỊ TRẢ LẠI
    public static final int GHTK_SHIP_RECEIVED_ORDER = 7;// ĐÃ GIAO CHO ĐƠN VỊ VÂN CHUYỂN
    public static final int CANCEL_GHTK = 8;// huỷ GHTK
    //trang thai thanh toan
    public static final int UNPAID = 2;
    public static final int PAID = 1;
    //khuyến mại
    public static final int PROMOTION=1;
    public static final int NOT_PROMOTION=2;

    public static String NEW_MEMBER = "NEW_MEMBER";
    public static String PAID_INF = "PAID";
    //Loại đơn
    public static final int BUY_AGENCY=1;
    public static final int BUY_STORE=2;
}
