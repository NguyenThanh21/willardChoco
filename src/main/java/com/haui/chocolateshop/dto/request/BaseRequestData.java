package com.haui.chocolateshop.dto.request;

import lombok.Data;

@Data
public class BaseRequestData<T extends IRequestData> {
    
    private String wsCode;
    private String sessionId;
    private String token;
    private T wsRequest;
}
