package com.haui.chocolateshop.dto.request.product;

import com.haui.chocolateshop.Model.Product;
import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

@Data
public class ProductRequest extends Product implements IRequestData {
    private int page;
    private int pageSize;
    @Override
    public boolean isValid() {
        return false;
    }
}
