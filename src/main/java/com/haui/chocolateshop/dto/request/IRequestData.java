package com.haui.chocolateshop.dto.request;

public interface IRequestData {
    boolean isValid();
}
