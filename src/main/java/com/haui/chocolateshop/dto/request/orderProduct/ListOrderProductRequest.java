package com.haui.chocolateshop.dto.request.orderProduct;

import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

import java.util.List;

@Data
public class ListOrderProductRequest implements IRequestData {
    private String cusName;
    private String phone;
    private String email;
    private String productName;
    private String address;
    private int payMethodId;
    List<OrderProductRequest> orderProductRequestList;
    @Override
    public boolean isValid() {
        return false;
    }
}
