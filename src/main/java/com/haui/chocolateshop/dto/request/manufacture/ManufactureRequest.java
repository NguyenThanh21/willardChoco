package com.haui.chocolateshop.dto.request.manufacture;

import com.haui.chocolateshop.Model.Manufacture;
import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

@Data
public class ManufactureRequest extends Manufacture implements IRequestData {
    private int page;
    private int pageSize;
    @Override
    public boolean isValid() {
        return false;
    }
}
