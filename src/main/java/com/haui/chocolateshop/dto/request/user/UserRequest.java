package com.haui.chocolateshop.dto.request.user;

import com.haui.chocolateshop.Model.User;
import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

@Data
public class UserRequest extends User implements IRequestData {
    private String passRetype;
    @Override
    public boolean isValid() {
        return false;
    }
}
