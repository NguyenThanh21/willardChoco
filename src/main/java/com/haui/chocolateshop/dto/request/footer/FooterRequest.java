package com.haui.chocolateshop.dto.request.footer;

import com.haui.chocolateshop.Model.Footer;
import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

@Data
public class FooterRequest extends Footer implements IRequestData {
    @Override
    public boolean isValid() {
        return false;
    }
}
