package com.haui.chocolateshop.dto.request.orderProduct;

import com.haui.chocolateshop.Model.OrderProduct;
import com.haui.chocolateshop.dto.request.IRequestData;
import lombok.Data;

@Data
public class OrderProductRequest extends OrderProduct implements IRequestData {
    private int categoryId;
    private int quantity;//số lượng
    private int idProduct;
    @Override
    public boolean isValid() {
        return false;
    }
}
