package com.haui.chocolateshop.dto.response.product;

import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

import java.util.List;

@Data
public class ListProductResponse implements IResponseData {
    private int totalPage;
    private int totalItem;
    List<ProductResponse> productResponseList;
}
