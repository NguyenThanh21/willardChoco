package com.haui.chocolateshop.dto.response.category;

import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

import java.util.List;
@Data
public class ListCategoryResponse implements IResponseData {
    List<CategoryResponse> categoryResponseList;
}
