package com.haui.chocolateshop.dto.response.user;

import com.haui.chocolateshop.Model.User;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

import java.util.Date;

@Data
public class UserInfoResponse extends User implements IResponseData {
    private String cusName;
    private String address;
    private int sex;
    private Date dateOfBirth;
}
