package com.haui.chocolateshop.dto.response;

import lombok.Data;


@Data
public class EmptyResponse implements IResponseData {
    int message;
}
