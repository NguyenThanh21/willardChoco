package com.haui.chocolateshop.dto.response.orderProduct;

import com.haui.chocolateshop.Model.OrderProduct;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class OrderProductResponse extends OrderProduct implements IResponseData {
    private String cusName;
    private String phone;
    private String email;
    private String productName;
}
