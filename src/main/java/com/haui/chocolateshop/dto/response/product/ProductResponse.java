package com.haui.chocolateshop.dto.response.product;

import com.haui.chocolateshop.Model.Product;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class ProductResponse extends Product implements IResponseData {
}
