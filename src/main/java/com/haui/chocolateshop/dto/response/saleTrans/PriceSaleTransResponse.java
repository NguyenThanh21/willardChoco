package com.haui.chocolateshop.dto.response.saleTrans;

import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class PriceSaleTransResponse implements IResponseData {
    private double amount;
    private int idProduct;
    private int promotion;
}
