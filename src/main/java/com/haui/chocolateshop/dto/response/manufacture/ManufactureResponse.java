package com.haui.chocolateshop.dto.response.manufacture;

import com.haui.chocolateshop.Model.Manufacture;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class ManufactureResponse extends Manufacture implements IResponseData {
}
