package com.haui.chocolateshop.dto.response.footer;

import com.haui.chocolateshop.Model.Footer;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class FooterResponse extends Footer implements IResponseData {
}
