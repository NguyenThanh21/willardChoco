package com.haui.chocolateshop.dto.response.user;

import com.haui.chocolateshop.Model.User;
import com.haui.chocolateshop.dto.request.IRequestData;
import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

@Data
public class UserResponse extends User implements IResponseData {
}
