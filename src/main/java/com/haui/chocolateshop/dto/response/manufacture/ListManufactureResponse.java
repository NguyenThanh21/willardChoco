package com.haui.chocolateshop.dto.response.manufacture;

import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

import java.util.List;

@Data
public class ListManufactureResponse implements IResponseData {
    private int totalPage;
    private int totalItem;
    List<ManufactureResponse> manufactureResponseList;
}
