package com.haui.chocolateshop.dto.response.user;

import com.haui.chocolateshop.dto.response.IResponseData;
import lombok.Data;

import java.util.List;
@Data
public class ListUserResponse implements IResponseData {
    List<UserResponse> userResponseList;
}
